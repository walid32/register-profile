<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once 'vendor/autoload.php'; // Email Verification Library for PHP " kickbox "
require_once 'vendor/ReCaptcha/autoload.php';

class profileController extends CI_Controller
{
	/* Api Keys for google reCaptcha */
	private $public_key = '6LcSxMcUAAAAACpbRjz1EHnjEanr3wIe5y-RFVd4';
	private $secret_key = '6LcSxMcUAAAAAEGCZClAncIacs7EJgzmB5rJBznH';

	/*  NOTE : This kickbox api key is valid only for 100 MAX attempts of email verification reason of insufficient account balance */
	private $kickbox_api_key = 'live_7106505ec058f09a4d6ae9b2c05815f59d8fb943278ec66fd08e72a52a09a6be';

	public $returned_data = null ;
	private  $respreCaptcha = true ;
	public $newuser = null ;

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('Profile_model');
	}

	/**
	 * Function index
	 * To load the main page
	 */
	public function index()
	{
		$data=array('public_key'=> $this->public_key) ;
		$this->load->view('profile\register',$data);
	}

	/**
	 * Function display
	 * Display list of users data
	 */
	public function display()
	{
		$data['result'] = $this->Profile_model->listData();
		$data['newuser'] = $this->newuser ;
		$this->load->view('profile\listprofile',$data);
	}

	/**
	 * Function registerProfile
	 * To register a new profile in the database
	 */
    public function registerProfile()
	{
		$recaptcha = new \ReCaptcha\ReCaptcha($this->secret_key);
		$resp = $recaptcha->verify($this->input->post('g-recaptcha-response'));
		$respreCaptcha = $resp->isSuccess();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('username','userName','required|is_unique[profile.username]');
		$this->form_validation->set_rules('email','email','required|is_unique[profile.email]|valid_email|callback_email_check'); // call email_check
		$this->form_validation->set_rules('g-recaptcha-response', 'recaptcha validation', 'required|callback_validate_captcha'); // call validate_captcha
		$form_valid = $this->form_validation->run();
		$this->newuser = $this->input->post('username');

		if(($form_valid) AND $resp->isSuccess() AND ($this->returned_data['valid'])) {
			$this->Profile_model->createData();
			$data['result'] = $this->Profile_model->listData();
			$data['newuser'] = $this->newuser ;
			$this->load->view('profile\listprofile',$data);
		}
		elseif(!$form_valid AND $resp->isSuccess() AND $this->returned_data['valid']){
			$key=array('public_key'=> $this->public_key) ;
			$this->load->view('profile\register',$key);
		}else{
			$key=array('public_key'=> $this->public_key) ;
			$this->load->view('profile\register',$key);
		}

	}

	/**
	 *  Function verifyEmail
	 *  Verify email using Kickbox api
	 *
	 * @param string $email
	 * @return array Returns two items, a boolean true if deliverable or false if undeliverable, risky or unknown
	 * and the result message
	 */
	public function verifyEmail($email)
	{
		$client = new Kickbox\Client($this->kickbox_api_key);
		$kickbox  = $client->kickbox();

		try {
			$response = $kickbox->verify($email, array('timeout' => 6000));
		}
		catch (Exception $e) {
			echo "Code: " . $e->getCode() . " Message: " . $e->getMessage();
		}

		if($response->body['result'] == "deliverable" )
		{
			$this->returned_data = array('valid'=>true,'result'=>$response->body['result']);
			return array('valid'=>true,'result'=>$response->body['result']);
		} else{
			$this->returned_data = array('valid'=>false,'result'=>$response->body['result']);
			return array('valid'=>false,'result'=>$response->body['result']);
		}


	}

	/**
	 * Function email_check
	 * To generate an error message after email verification
	 * @param $data
	 * @return bool
	 */
	public function email_check($data)
	{
		$data = $this->verifyEmail($data);
		if ($data['valid'] == false)
		{

			$this->form_validation->set_message('email_check', 'The email is '.$data['result']);
			return FALSE;
		}
		else
		{

			return TRUE;
		}
	}
	/**
	 * Function validate_captcha
	 * To generate an error message after reCAPTCHA verification
	 * @param $respreCaptcha
	 * @return bool
	 */
	public function validate_captcha($respreCaptcha)
	{

		if ($respreCaptcha == false)
		{
			$this->form_validation->set_message('validate_captcha', 'Please check the the reCAPTCHA form');
			return FALSE;
		}
		else
		{

			return TRUE;
		}
	}



	/**
	 * Function valid
	 * JUST To verify the  kickbox_api_key
	 */
	public function valid()
	{
		$client = new Kickbox\Client($this->kickbox_api_key);
		$kickbox  = $client->kickbox();

		try {
			$response = $kickbox->verify("walidromdhani32@gmail.com");
		}
		catch (Exception $e) {
			echo "Code: " . $e->getCode() . " Message: " . $e->getMessage();
		}
		var_dump($response);
	}


}
