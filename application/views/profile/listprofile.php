<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->
	<link rel="stylesheet" href="<?=base_url();?>application/views/assets/css/bootstrap.min.css">

	<title>List of profile</title>
</head>
<body>
<nav class="navbar navbar-dark bg-primary">
	<a class="navbar-brand " href="<?php echo site_url('profileController/')?>">Home</a>
	<ul class="navbar-nav mr-auto">
		<li class="nav-item">
			<a class="navbar-brand" href="<?php echo site_url('profileController/display')?>">All Users</a>
		</li>
	</ul>
</nav>

<div class="container mt-5" >

	<?php
	if( !empty($newuser))
	{
		echo '<div class="jumbotron">
                <div class="section">
                  <h1 class="text-center text-primary">Welcome, '.$newuser.' thanks for your registration </h1>
                </div>
              </div>';
	}
	?>

	<table class="table">
		<thead class="thead-dark">
		<tr>
			<th scope="col">#</th>
			<th scope="col">UserName</th>
			<th scope="col">Email</th>

		</tr>
		</thead>
		<tbody>
		<?php $i=1;
		foreach( $result as $field) {
		?>
		<tr>
			<th scope="row"><?php echo $i++; ?></th>
			<td><?php echo $field->username ?></td>
			<td><?php echo $field->email ?></td>

		</tr>
		<?php }?>

		</tbody>
	</table>
</div>

</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script> -->
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>  -->

<script src="<?=base_url();?>application/views/assets/js/jquery-3.4.1.slim.min.js" ></script>
<script src="<?=base_url();?>application/views/assets/js/popper.min.js" ></script>
<script src="<?=base_url();?>application/views/assets/js/bootstrap.min.js" ></script>
</body>
</html>
