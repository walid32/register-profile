<!doctype html>
<html lang="en">
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- Bootstrap CSS -->
	<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"> -->
	<link rel="stylesheet" href="<?=base_url();?>application/views/assets/css/bootstrap.min.css">
	<!-- reCAPTCHA -->
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>
	<title>Home</title>
</head>
<body>

<nav class="navbar navbar-dark bg-primary">
	<a class="navbar-brand " href="<?php echo site_url('profileController/')?>">Home</a>
	<ul class="navbar-nav mr-auto">
		<li class="nav-item">
			<a class="navbar-brand" href="<?php echo site_url('profileController/display')?>">All Users</a>
		</li>
	</ul>
	<button type="button" class="btn btn-primary my-2 my-sm-0" data-toggle="modal" data-target="#exampleModal">Register with Modal</button>
</nav>
<p class="text-lg-right"> Please click here to load modal other time       .  </p>
<div class="container">
	<br><br><br>
	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Create your account</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<!-- To display error messages detect  -->
					<?php echo validation_errors('<div class="alert alert-danger">','</div>');  ?>

					<form method="POST" action="<?php echo site_url('profileController/registerProfile');?>">
						<div class="form-group">
							<label for="exampleInputEmail1">Username</label>
							<input type="text" class="form-control" name="username" value="<?php echo set_value('username'); ?>" aria-describedby="emailHelp">
							<small id="emailHelp" class="form-text text-muted">We'll never share your USERNAME with anyone else.</small>
						</div>
						<div class="form-group">
							<label for="exampleInputEmail1">Email address </B></FONT></label>
							<input type="email" class="form-control" name="email" value="<?php echo set_value('email'); ?>" aria-describedby="emailHelp">
							<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
						</div>
						<div class="g-recaptcha" data-sitekey="<?php  echo $public_key; ?>"></div>
						<button type="submit" class="btn btn-primary mt-3" value="save">Submit</button>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

				</div>
			</div>
		</div>
	</div>
</div>

<!-- Normal Form -->

<div class="container">

	<div class="col-md-6">
		<h3>Simple form  </h3>
	<h1>Create your account </h1>

	<?php echo validation_errors('<div class="alert alert-danger">','</div>'); ?>
	<form method="post" action="<?php echo site_url('profileController/registerProfile'); ?>">
		<div class="form-group">
		<label for="exampleInputEmail1">Username</label>
		<input type="text" class="form-control" name="username"  value="<?php echo set_value('username'); ?>" aria-describedby="emailHelp">
		<small id="emailHelp" class="form-text text-muted">We'll never share your USERNAME with anyone else.</small>
        </div>
    <div class="form-group">
	<label for="exampleInputEmail1">Email address </label>

	<input type="email" class="form-control" name="email" value="<?php echo set_value('email'); ?>" aria-describedby="emailHelp">
	<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
     <div class="g-recaptcha " data-sitekey="6LcSxMcUAAAAACpbRjz1EHnjEanr3wIe5y-RFVd4"></div>
     <button type="submit" class="btn btn-primary mt-3 " value="save">Submit</button>
     </form>
	 </div>
</div>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script> -->
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>  -->

<script src="<?=base_url();?>application/views/assets/js/jquery-3.4.1.slim.min.js" ></script>
<script src="<?=base_url();?>application/views/assets/js/popper.min.js" ></script>
<script src="<?=base_url();?>application/views/assets/js/bootstrap.min.js" ></script>

<!-- To display the form with class Modal directly -->

<script src="<?=base_url();?>application/views/assets/js/custom.js" ></script>

</body>
</html>
