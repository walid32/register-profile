<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Profile_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	/**
	 * Function createData
	 * To register a new profile in the database
	 */
	public function createData()
	{
			$data = array(
				'username' => $this->input->post('username'),
				'email' => $this->input->post('email')
			);
			$this->db->insert('profile',$data);
	}


	/**
	 * Function listData
	 * @return  All data profile
	 */
	public function listData()
	{
		$query = $this->db->query('SELECT * FROM profile');
		return $query->result();

	}
}
